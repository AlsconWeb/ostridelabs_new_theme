<?php
/**
 * Our Services Tabs.
 *
 * @package alexl/ostridelabs
 */

namespace OSTD\WPBakery\component;

/**
 * ServiceTabs class file.
 */
class ServiceTabs {
	/**
	 * ServiceTabs construct.
	 */
	public function __construct() {
		add_shortcode( 'ostd_service_tabs', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ostd_service_tabs', [ $this, 'map' ] );
		}
	}

	/**
	 * Output template.
	 *
	 * @param array       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();

		include get_template_directory() . '/src/php/WPBakery/template/ServiceTabs/template.php';

		return ob_get_clean();
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Services Tabs', 'ostd' ),
			'description'             => esc_html__( 'Services Tabs', 'ostd' ),
			'base'                    => 'ostd_service_tabs',
			'category'                => __( 'OSTD', 'ostd' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'param_group',
					'param_name'  => 'services',
					'value'       => '',
					'heading'     => __( 'Services', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
					'params'      => [
						[
							'type'        => 'textfield',
							'param_name'  => 'tab_id',
							'value'       => '',
							'heading'     => __( 'Tab ID', 'ostd' ),
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
						],
						[
							'type'        => 'textfield',
							'param_name'  => 'tab_name',
							'value'       => '',
							'heading'     => __( 'Tab title', 'ostd' ),
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
						],
						[
							'type'        => 'attach_image',
							'param_name'  => 'image',
							'value'       => '',
							'heading'     => __( 'Image', 'ostd' ),
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
						],
						[
							'type'        => 'attach_image',
							'param_name'  => 'image_hover',
							'value'       => '',
							'heading'     => __( 'Image Hover', 'ostd' ),
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
						],
						[
							'type'        => 'vc_link',
							'param_name'  => 'link',
							'value'       => '',
							'heading'     => __( 'Url Service', 'ostd' ),
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
						],
						[
							'type'        => 'textarea',
							'param_name'  => 'description',
							'value'       => '',
							'heading'     => __( 'Description Service', 'ostd' ),
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
						],

					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'ostd' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'ostd' ),
				],
			],
		];
	}
}
