<?php
/**
 * WP Bakery components.
 * Testimonials.
 *
 * @package alexl/ostridelabs
 */

namespace OSTD\WPBakery\component;

/**
 * Testimonials class file.
 */
class Testimonials {
	/**
	 * Testimonials construct.
	 */
	public function __construct() {
		add_shortcode( 'ostd_testimonials', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ostd_testimonials', [ $this, 'map' ] );
		}
	}

	/**
	 * Output template.
	 *
	 * @param array       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include get_template_directory() . '/src/php/WPBakery/template/Testimonials/template.php';

		return ob_get_clean();
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Testimonials', 'ostd' ),
			'description'             => esc_html__( 'Testimonials', 'ostd' ),
			'base'                    => 'ostd_testimonials',
			'category'                => __( 'OSTD', 'ostd' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'dropdown',
					'param_name'  => 'style',
					'value'       => [
						__( 'Slider', 'ostd' )          => 1,
						__( 'One Testimonial', 'ostd' ) => 2,
					],
					'heading'     => __( 'Style output', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'param_name'  => 'title',
					'value'       => '',
					'heading'     => __( 'Title ', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
					'dependency'  => [
						'element' => 'style',
						'value'   => '2',
					],
				],
				[
					'type'        => 'textfield',
					'param_name'  => 'post_id',
					'value'       => '',
					'heading'     => __( 'Testimonial ID ', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
					'dependency'  => [
						'element' => 'style',
						'value'   => '2',
					],
				],
				[
					'type'        => 'textfield',
					'param_name'  => 'post_not_in',
					'value'       => '',
					'heading'     => __( 'Exclude review', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
					'dependency'  => [
						'element' => 'style',
						'value'   => '1',
					],
				],
				[
					'type'        => 'textfield',
					'param_name'  => 'count',
					'value'       => '',
					'heading'     => __( 'Count output', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
					'dependency'  => [
						'element' => 'style',
						'value'   => '1',
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'ostd' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'ostd' ),
				],
			],
		];
	}
}
