<?php
/**
 * Scale DB Animation Block.
 *
 * @package alexl/ostridelabs
 */

namespace OSTD\WPBakery\component;

/**
 * ScaleBDAnimation class file.
 */
class ScaleBDAnimation {
	/**
	 * ScaleBDAnimation construct.
	 */
	public function __construct() {
		add_shortcode( 'ostd_scale_db_block', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ostd_scale_db_block', [ $this, 'map' ] );
		}
	}

	/**
	 * Output template.
	 *
	 * @param array       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include get_template_directory() . '/src/php/WPBakery/template/ScaleBDAnimation/template.php';

		return ob_get_clean();
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Scale DB text', 'ostd' ),
			'description'             => esc_html__( 'Scale DB text', 'ostd' ),
			'base'                    => 'ostd_scale_db_block',
			'category'                => __( 'OSTD', 'ostd' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textfield',
					'param_name'  => 'title',
					'value'       => '',
					'heading'     => __( 'Title', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'param_name'  => 'sub_title',
					'value'       => '',
					'heading'     => __( 'Sub Title', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'param_name'  => 'block_title',
					'value'       => '',
					'heading'     => __( 'Block Title', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'param_group',
					'param_name'  => 'param_text',
					'value'       => '',
					'heading'     => __( 'Description Table', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
					'params'      => [
						[
							'type'        => 'textfield',
							'param_name'  => 'item',
							'value'       => '',
							'heading'     => __( 'Item text', 'ostd' ),
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'ostd' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'ostd' ),
				],
			],
		];
	}
}
