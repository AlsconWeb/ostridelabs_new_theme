<?php
/**
 * Arrow Block Animation.
 *
 * @package alexl/ostridelabs
 */

namespace OSTD\WPBakery\component;
/**
 * ArrowBlock class file.
 */
class ArrowBlock {
	/**
	 * ArrowBlock construct.
	 */
	public function __construct() {
		add_shortcode( 'ostd_arrow_block_animation', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ostd_arrow_block_animation', [ $this, 'map' ] );
		}
	}

	/**
	 * Output template.
	 *
	 * @param array       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include get_template_directory() . '/src/php/WPBakery/template/ArrowBlock/template.php';

		return ob_get_clean();
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Arrow Animation Block', 'ostd' ),
			'description'             => esc_html__( 'Arrow Animation Block', 'ostd' ),
			'base'                    => 'ostd_arrow_block_animation',
			'category'                => __( 'OSTD', 'ostd' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'param_group',
					'param_name'  => 'arrow_text',
					'value'       => '',
					'heading'     => __( 'Arrow step text', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
					'params'      => [
						[
							'type'        => 'textfield',
							'param_name'  => 'item_text',
							'value'       => '',
							'heading'     => __( 'Item text', 'ostd' ),
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
						],
					],
				],
				[
					'type'        => 'colorpicker',
					'param_name'  => 'first_color',
					'value'       => '',
					'heading'     => __( 'First Color', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'colorpicker',
					'param_name'  => 'second_color',
					'value'       => '',
					'heading'     => __( 'Second Color', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'ostd' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'ostd' ),
				],
			],
		];
	}
}
