<?php
/**
 * Security SVG Animation Block.
 *
 * @package alexl/ostridelabs
 */

namespace OSTD\WPBakery\component;

/**
 * SecuritySVGAnimation class file.
 */
class SecuritySVGAnimation {
	/**
	 * CodeBlockAnimation construct.
	 */
	public function __construct() {
		add_shortcode( 'ostd_security_svg_animation', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ostd_security_svg_animation', [ $this, 'map' ] );
		}
	}

	/**
	 * Output template.
	 *
	 * @param array       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include get_template_directory() . '/src/php/WPBakery/template/SecuritySVGAnimation/template.php';

		return ob_get_clean();
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Security Block Animation', 'ostd' ),
			'description'             => esc_html__( 'Security Block Animation', 'ostd' ),
			'base'                    => 'ostd_security_svg_animation',
			'category'                => __( 'OSTD', 'ostd' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textarea_raw_html',
					'param_name'  => 'svg',
					'value'       => '',
					'heading'     => __( 'SVG Text', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'ostd' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'ostd' ),
				],
			],
		];
	}
}
