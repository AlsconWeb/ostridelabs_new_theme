<?php
/**
 * Cloud Image Block.
 *
 * @package alexl/ostridelabs
 */

namespace OSTD\WPBakery\component;

/**
 * CloudBlock class file.
 */
class CloudBlock {
	/**
	 * CloudBlock construct.
	 */
	public function __construct() {
		add_shortcode( 'ostd_cloud_images_block', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ostd_cloud_images_block', [ $this, 'map' ] );
		}
	}

	/**
	 * Output template.
	 *
	 * @param array       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include get_template_directory() . '/src/php/WPBakery/template/CloudBlock/template.php';

		return ob_get_clean();
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Cloud Images', 'ostd' ),
			'description'             => esc_html__( 'Cloud Images', 'ostd' ),
			'base'                    => 'ostd_cloud_images_block',
			'category'                => __( 'OSTD', 'ostd' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'param_group',
					'param_name'  => 'images',
					'value'       => '',
					'heading'     => __( 'Steps', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
					'params'      => [
						[
							'type'        => 'attach_image',
							'param_name'  => 'image',
							'value'       => '',
							'heading'     => __( 'Image SVG', 'ostd' ),
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
						],

					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'ostd' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'ostd' ),
				],
			],
		];
	}
}
