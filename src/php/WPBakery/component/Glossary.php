<?php
/**
 * Glossary.
 *
 * @package alexl/ostridelabs
 */

namespace OSTD\WPBakery\component;

/**
 * Glossary class file.
 */
class Glossary {
	/**
	 * Glossary construct.
	 */
	public function __construct() {
		add_shortcode( 'ostd_glossary', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ostd_glossary', [ $this, 'map' ] );
		}
	}

	/**
	 * Output template.
	 *
	 * @param array       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();

		include get_template_directory() . '/src/php/WPBakery/template/Glossary/template.php';

		return ob_get_clean();
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Glossary', 'ostd' ),
			'description'             => esc_html__( 'Glossary', 'ostd' ),
			'base'                    => 'ostd_glossary',
			'category'                => __( 'OSTD', 'ostd' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'param_group',
					'param_name'  => 'glossary',
					'value'       => '',
					'heading'     => __( 'Glossary elements', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
					'params'      => [
						[
							'type'        => 'textfield',
							'param_name'  => 'letter',
							'value'       => '',
							'heading'     => __( 'Letter', 'ostd' ),
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
						],
						[
							'type'        => 'param_group',
							'param_name'  => 'items',
							'value'       => '',
							'heading'     => __( 'Title key', 'ostd' ),
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
							'params'      => [
								[
									'type'        => 'vc_link',
									'param_name'  => 'link',
									'value'       => '',
									'heading'     => __( 'Ссылка на полную статью', 'ostd' ),
									'admin_label' => false,
									'save_always' => true,
									'group'       => 'General',
								],
							],
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'ostd' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'ostd' ),
				],
			],
		];
	}
}