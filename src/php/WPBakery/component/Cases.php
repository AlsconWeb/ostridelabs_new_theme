<?php
/**
 * WP Bakery components.
 * Cases.
 *
 * @package alexl/ostridelabs
 */

namespace OSTD\WPBakery\component;

/**
 * Cases class file.
 */
class Cases {
	/**
	 * Cases construct.
	 */
	public function __construct() {
		add_shortcode( 'ostd_cases', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ostd_cases', [ $this, 'map' ] );
		}
	}

	/**
	 * Output template.
	 *
	 * @param array       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		if ( '1' === $atts['style'] ) {
			include get_template_directory() . '/src/php/WPBakery/template/Cases/style-one-template.php';
		}

		if ( '2' === $atts['style'] ) {
			include get_template_directory() . '/src/php/WPBakery/template/Cases/style-two-template.php';
		}

		return ob_get_clean();
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Cases', 'ostd' ),
			'description'             => esc_html__( 'Cases', 'ostd' ),
			'base'                    => 'ostd_cases',
			'category'                => __( 'OSTD', 'ostd' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'dropdown',
					'param_name'  => 'style',
					'value'       => [
						__( 'Defaul Style', 'ostd' )   => 1,
						__( 'Category Block', 'ostd' ) => 2,
					],
					'heading'     => __( 'Style output', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'textfield',
					'param_name'  => 'count',
					'value'       => '',
					'heading'     => __( 'Count output', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
					'dependency'  => [
						'element' => 'style',
						'value'   => [ '1' ],
					],
				],
				[
					'type'        => 'textfield',
					'param_name'  => 'button_text',
					'value'       => '',
					'heading'     => __( 'Text Button', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
					'dependency'  => [
						'element' => 'style',
						'value'   => [ '1' ],
					],
				],
				[
					'type'        => 'dropdown',
					'param_name'  => 'categories',
					'value'       => self::get_category_select(),
					'heading'     => __( 'Style output', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
					'dependency'  => [
						'element' => 'style',
						'value'   => [ '2' ],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'ostd' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'ostd' ),
				],
			],
		];
	}

	/**
	 * Get Category Cases.
	 *
	 * @return array
	 */
	private static function get_category_select(): array {
		$terms = get_terms( [
			'taxonomy'   => 'category-cases',
			'hide_empty' => false,
		] );

		$categories = [];

		if ( ! empty( $terms ) ) {
			foreach ( $terms as $term ) {
				$categories[ $term->name ] = $term->term_id;
			}
		}

		return $categories;
	}
}
