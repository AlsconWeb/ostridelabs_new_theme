<?php
/**
 * FAQ block.
 *
 * @package alexl/ostridelabs
 */

namespace OSTD\WPBakery\component;
/**
 * FAQ class file.
 */
class FAQ {
	/**
	 * FAQ construct.
	 */
	public function __construct() {
		add_shortcode( 'ostd_faq', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ostd_faq', [ $this, 'map' ] );
		}
	}

	/**
	 * Output template.
	 *
	 * @param array       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();

		include get_template_directory() . '/src/php/WPBakery/template/FAQ/template.php';

		return ob_get_clean();
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'FAQ', 'ostd' ),
			'description'             => esc_html__( 'FAQ', 'ostd' ),
			'base'                    => 'ostd_faq',
			'category'                => __( 'OSTD', 'ostd' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'param_group',
					'param_name'  => 'faq_items',
					'value'       => '',
					'heading'     => __( 'FAQ', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
					'params'      => [
						[
							'type'        => 'textfield',
							'param_name'  => 'title',
							'value'       => '',
							'heading'     => __( 'Title', 'ostd' ),
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
						],
						[
							'type'        => 'textarea',
							'param_name'  => 'description',
							'value'       => '',
							'heading'     => __( 'Description', 'ostd' ),
							'admin_label' => false,
							'save_always' => true,
							'group'       => 'General',
						],
					],
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'ostd' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'ostd' ),
				],
			],
		];
	}
}
