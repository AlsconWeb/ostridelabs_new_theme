<?php
/**
 * WP Bakery components.
 * Opportunities
 *
 * @package alexl/ostridelabs
 */

namespace OSTD\WPBakery\component;

/**
 * Opportunities class file.
 */
class Opportunities {

	/**
	 * Opportunities construct.
	 */
	public function __construct() {
		add_shortcode( 'ostd_opportunities', [ $this, 'output' ] );

		// Map shortcode to Visual Composer.
		if ( function_exists( 'vc_lean_map' ) ) {
			vc_lean_map( 'ostd_opportunities', [ $this, 'map' ] );
		}
	}

	/**
	 * Output template.
	 *
	 * @param array       $atts    Attributes.
	 * @param string|null $content Content.
	 *
	 * @return string
	 */
	public function output( $atts, string $content = null ): string {
		ob_start();
		include get_template_directory() . '/src/php/WPBakery/template/Opportunities/template.php';

		return ob_get_clean();
	}

	/**
	 * Map field.
	 *
	 * @return array
	 */
	public function map(): array {
		return [
			'name'                    => esc_html__( 'Opportunities', 'ostd' ),
			'description'             => esc_html__( 'Opportunities', 'ostd' ),
			'base'                    => 'ostd_opportunities',
			'category'                => __( 'OSTD', 'ostd' ),
			'show_settings_on_create' => false,
			'icon'                    => '',
			'params'                  => [
				[
					'type'        => 'textfield',
					'param_name'  => 'title_job',
					'value'       => '',
					'heading'     => __( 'Title Job', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'textarea',
					'param_name'  => 'time',
					'value'       => '',
					'heading'     => __( 'Time', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'        => 'textarea_html',
					'param_name'  => 'description',
					'value'       => '',
					'heading'     => __( 'Content', 'ostd' ),
					'admin_label' => false,
					'save_always' => true,
					'group'       => 'General',
				],
				[
					'type'       => 'css_editor',
					'heading'    => esc_html__( 'CSS box', 'ostd' ),
					'param_name' => 'css',
					'group'      => esc_html__( 'Design Options', 'ostd' ),
				],
			],
		];
	}
}
