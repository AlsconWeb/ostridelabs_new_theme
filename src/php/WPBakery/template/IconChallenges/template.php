<?php
/**
 * Template Icon Challenges.
 *
 * @package OSTD\WPBakery\component
 */

$css_class      = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$icon_challenge = vc_param_group_parse_atts( $atts['icon_challenges'] );

if ( ! empty( $icon_challenge ) ) :
	?>
	<div class="descriptions <?php echo esc_attr( $css_class ?? null ); ?>">
		<?php foreach ( $icon_challenge as $item ) : ?>
			<div class="description-item <?php echo esc_attr( $item['icon_name'] ) ?? ''; ?>">
				<h3 class="title"><?php echo esc_html( $item['title'] ) ?? ''; ?></h3>
				<?php echo wp_kses_post( wpautop( $item['description'] ) ) ?? ''; ?>
			</div>
		<?php endforeach; ?>
	</div>
<?php
endif;
