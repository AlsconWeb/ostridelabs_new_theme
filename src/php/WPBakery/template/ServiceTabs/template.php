<?php
/**
 * ServiceTabs template.
 *
 * @package OSTD\WPBakery\component
 */


$services  = vc_param_group_parse_atts( $atts['services'] );
$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
?>
<div class="<?php echo esc_attr( $css_class ) ?? ''; ?>">
	<?php if ( ! empty( $services ) ): ?>
		<ul class="nav nav-tabs">
			<?php foreach ( $services as $key => $service ): ?>
				<li class="<?php echo 0 === $key ? 'active' : ''; ?>">
					<a href="#<?php echo esc_attr( $service['tab_id'] ) ?? ''; ?>" data-toggle="tab">
						<?php echo esc_html( $service['tab_name'] ); ?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>

		<div class="tab-content">
			<?php
			foreach ( $services as $key => $service ) :
				$link = vc_build_link( $service['link'] );
				?>
				<div
						class="tab-pane <?php echo 0 === $key ? 'active' : ''; ?>"
						id="<?php echo esc_attr( $service['tab_id'] ) ?? ''; ?>">
					<a href="<?php echo esc_url( $link['url'] ); ?>">
						<div class="img">
							<?php
							$image_url       = wp_get_attachment_url( $service['image'] );
							$image_hover_url = wp_get_attachment_url( $service['image_hover'] );
							?>

							<img
									src="<?php echo esc_url( $image_url ) ?? ''; ?>"
									alt="<?php echo esc_html( get_the_title( $service['image'] ) ); ?>">
							<img
									src="<?php echo esc_url( $image_hover_url ) ?? ''; ?>"
									alt="<?php echo esc_html( get_the_title( $service['image_hover'] ) ); ?>">
						</div>
						<p class="title">
							<?php echo wp_kses( wpautop( $service['description'] ), [ 'br' => [] ] ) ?? ''; ?>
						</p>
						<i class="icon-right button"><?php esc_attr_e( 'See More', 'ostd' ); ?></i>
					</a>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>
