<?php
/**
 * Template Step by Step.
 * Style two.
 *
 * @package OSTD\WPBakery\component
 */

$steps     = vc_param_group_parse_atts( $atts['steps'] );
$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );

$last_steps = count( $steps ) - 2;
?>

<div class="steps-items <?php echo esc_attr( $css_class ) ?? ''; ?>">
	<p class="step-title"><?php echo esc_html( $atts['head_line'] ); ?></p>
	<?php if ( ! empty( $steps ) ) : ?>
		<?php foreach ( $steps as $key => $item ) : ?>
			<?php if ( $key < $last_steps ): ?>
				<div class="item">
					<p class="head-line">
						<?php echo wp_kses_post( $item['title'] ) ?? ''; ?>
						<i class="<?php echo esc_attr( $item['icon_name'] ) ?? ''; ?>"></i>
					</p>
					<div class="desc">
						<p><?php echo wp_kses_post( $item['description'] ) ?></p>
					</div>
					<?php
					$step_dec = vc_param_group_parse_atts( $item['description_steps'] );
					?>
					<div class="items">
						<?php foreach ( $step_dec as $step ) : ?>
							<div class="description">
								<p class="sub-title"><?php echo esc_html( $step['title'] ); ?></p>
								<p>
									<?php echo wp_kses_post( $step['description'] ); ?>
								</p>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php else: ?>
				<div class="item">
					<p class="head-line">
						<?php echo wp_kses_post( $item['title'] ) ?? ''; ?>
						<i class="<?php echo esc_attr( $item['icon_name'] ) ?? ''; ?>"></i>
					</p>
					<div class="items">
						<?php
						$step_dec = vc_param_group_parse_atts( $item['description_steps'] );
						?>
						<div class="desc">
							<p><?php echo wp_kses_post( $item['description'] ) ?></p>
						</div>
						<?php foreach ( $step_dec as $step ) : ?>
							<div class="description">
								<p class="sub-title"><?php echo esc_html( $step['title'] ); ?></p>
								<p>
									<?php echo wp_kses_post( $step['description'] ); ?>
								</p>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
</div>
