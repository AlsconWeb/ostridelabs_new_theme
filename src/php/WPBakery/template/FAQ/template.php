<?php
/**
 * FAQ template.
 *
 * @package OSTD\WPBakery\component
 */

$faq_items = vc_param_group_parse_atts( $atts['faq_items'] );
$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
?>
<div class="faq-items <?php echo esc_attr( $css_class ) ?? ''; ?>">
	<?php
	if ( ! empty( $faq_items ) ) {
		foreach ( $faq_items as $item ) {
			?>
			<div class="faq-item" itemscope itemprop="mainEntity" itemtype="https://schema.org/Question">
				<p class="icon-arrow-down" itemprop="name"><?php echo esc_html( $item['title'] ); ?></p>
				<div class="hide" itemscope itemprop="acceptedAnswer" itemtype="https://schema.org/Answer">
					<div itemprop="text">
						<?php echo wp_kses_post( wpautop( $item['description'] ) ); ?>
					</div>
				</div>
			</div>
			<?php
		}
	}
	?>
</div>
