<?php
/**
 * Arrow Block Animation template.
 *
 * @package OSTD\WPBakery\component
 */

$text         = vc_param_group_parse_atts( $atts['arrow_text'] );
$first_color  = $atts['first_color'] ?? '';
$second_color = $atts['second_color'] ?? '';
$css_class    = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
?>
<div class="arrow-block">
	<ul class="arrow-items">
		<li>
			<span style="color:<?php echo esc_attr( $first_color ); ?>;"></span>
			<span style="color:<?php echo esc_attr( $first_color ); ?>;"></span>
			<span style="color:<?php echo esc_attr( $first_color ); ?>;"></span>
			<span style="color:<?php echo esc_attr( $second_color ); ?>;"></span>
		</li>
		<li>
			<span style="color:<?php echo esc_attr( $first_color ); ?>;"></span>
			<span style="color:<?php echo esc_attr( $first_color ); ?>;"></span>
			<span style="color:<?php echo esc_attr( $second_color ); ?>;"></span>
			<span style="color:<?php echo esc_attr( $first_color ); ?>;"></span>
		</li>
	</ul>
	<?php if ( ! empty( $text ) ) : ?>
		<?php foreach ( $text as $item ) : ?>
			<p><?php echo esc_html( $item['item_text'] ); ?></p>
		<?php endforeach; ?>
	<?php endif; ?>
</div>
