<?php
/**
 * CloudBlock template.
 *
 * @package OSTD\WPBakery\component
 */

$cloud_images = vc_param_group_parse_atts( $atts['images'] );
$css_class    = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );

if ( ! empty( $cloud_images ) ) :
	?>
	<ul class="cloud-items <?php echo esc_attr( $css_class ?? '' ); ?>">
		<?php
		foreach ( $cloud_images as $image ) :
			$url = wp_get_attachment_url( $image['image'] );
			?>
			<li class="cloud-item">
				<a href="#form">
					<img
							src="<?php echo esc_url( $url ); ?>"
							alt="<?php echo esc_attr( get_the_title( $image['image'] ) ); ?>">
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
<?php endif; ?>
