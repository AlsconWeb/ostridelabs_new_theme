<?php
/**
 * Glossary template.
 *
 * @package alexl/ostridelabs
 */

$items = vc_param_group_parse_atts( $atts['glossary'] );
?>
	<ul class="glossary-nav">
		<li><a href="#a"><?php esc_html_e( 'A - D', 'ostd' ); ?></a></li>
		<li><a href="#e"><?php esc_html_e( 'E - H', 'ostd' ); ?></a></li>
		<li><a href="#i"><?php esc_html_e( 'I - L', 'ostd' ); ?></a></li>
		<li><a href="#m"><?php esc_html_e( 'M - P', 'ostd' ); ?></a></li>
		<li><a href="#q"><?php esc_html_e( 'Q - P', 'ostd' ); ?></a></li>
		<li><a href="#u"><?php esc_html_e( 'U - X', 'ostd' ); ?></a></li>
		<li><a href="#y"><?php esc_html_e( 'Y - Z', 'ostd' ); ?></a></li>
	</ul>
<?php
if ( ! empty( $items ) ) {
	?>
	<ul class="glossary-items">
		<?php foreach ( $items as $item ) { ?>
			<li
					data-symbol="<?php echo esc_html( $item['letter'] ); ?>"
					id="<?php echo esc_html( strtolower( $item['letter'] ) ); ?>">
				<?php
				$glossary = vc_param_group_parse_atts( $item['items'] );
				if ( ! empty( $glossary ) ) {
					foreach ( $glossary as $value ) {
						$link_glossary = vc_build_link( $value['link'] );
						?>
						<a
								href="<?php echo ! empty( $link_glossary['url'] ) ? esc_url( $link_glossary['url'] ) : '#'; ?>"
								target="<?php echo esc_attr( $link_glossary['target'] ); ?>"
								rel="<?php echo esc_attr( $link_glossary['rel'] ); ?>">
							<?php echo esc_html( $link_glossary['title'] ); ?>
						</a>
						<?php
					}
				}
				?>
			</li>
		<?php } ?>
	</ul>
	<?php
}
