<?php
/**
 *  Code Block Animation.
 *
 * @package OSTD\WPBakery\component
 */

$description    = vc_param_group_parse_atts( $atts['code_description'] );
$icon_lock_text = vc_param_group_parse_atts( $atts['icon_lock_text'] );
$css_class      = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
?>

<div class="code-block <?php echo esc_attr( $css_class ) ?? ''; ?>">
	<?php if ( ! empty( $description ) ) : ?>
		<ul class="code-desc">
			<?php foreach ( $description as $item ) : ?>
				<li><?php echo esc_html( $item['item_text'] ); ?></li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
	<?php if ( ! empty( $icon_lock_text ) ) : ?>
		<ul class="icon-lock">
			<?php foreach ( $icon_lock_text as $item ) : ?>
				<li><?php echo esc_html( $item['item_text'] ); ?></li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
</div>
