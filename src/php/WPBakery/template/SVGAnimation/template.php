<?php
/**
 * SVGAnimation template.
 *
 * @package OSTD\WPBakery\component
 */

$svg_menu  = vc_param_group_parse_atts( $atts['svg_menu'] );
$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );

// phpcs:disable
$svg = ! empty( $atts['svg'] ) ? rawurldecode( base64_decode( wp_strip_all_tags( $atts['svg'] ) ) ) : '';
// phpcs:enable


if ( 0 !== count( $svg_menu ) ) :
	?>
	<ul class="menu-svg <?php echo esc_html( $css_class ) ?? ''; ?>">
		<?php foreach ( $svg_menu as $item_menu ) : ?>
			<li><?php echo esc_html( $item_menu['item_name'] ); ?></li>
		<?php endforeach; ?>
	</ul>
<?php
endif;
// phpcs:disable
if ( $svg ) {
	echo $svg;
}
// phpcs:enable


