<?php
/**
 * Cases two template.
 *
 * @package  OSTD\WPBakery\component
 */

$args = [
	'post_type'      => 'cases',
	'posts_per_page' => 3,
	'post_status'    => 'publish',
	'tax_query'      => [
		[
			'taxonomy' => 'category-cases',
			'field'    => 'id',
			'terms'    => $atts['categories'],
		],
	],
	'orderby'        => 'date',
	'order'          => 'DESC',
];

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );

$query     = new WP_Query( $args );
$term_name = get_term_by( 'id', $atts['categories'], 'category-cases' );

if ( $query->have_posts() ) :
	?>
	<h2 class="title"><?php echo esc_html( $term_name->name ) ?? ''; ?></h2>
	<?php
	while ( $query->have_posts() ) :
		$query->the_post();
		$case_id = get_the_ID();
		?>
		<div class="cases-item <?php echo esc_html( $css_class ) ?? ''; ?>">
			<a class="link" href="<?php the_permalink(); ?>"></a>
			<div class="cases-head">
				<?php if ( has_post_thumbnail( $case_id ) ) : ?>
					<?php the_post_thumbnail( 'blog_big' ); ?>
				<?php else : ?>
					<img src="//via.placeholder.com/600x390" alt="No Image">
				<?php endif; ?>
			</div>
			<div class="cases-footer">
				<p class="head-line"><?php the_title(); ?></p>
				<p><?php echo wp_kses_post( get_the_excerpt() ) ?? ''; ?></p>
				<p class="more"><?php esc_attr_e( 'More', 'ostd' ); ?></p>
			</div>
		</div>
	<?php
	endwhile;
	wp_reset_postdata();
endif;
