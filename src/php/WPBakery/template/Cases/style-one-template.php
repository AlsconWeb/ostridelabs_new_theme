<?php
/**
 * Cases one template.
 *
 * @package  OSTD\WPBakery\component
 */

$args = [
	'post_type'      => 'cases',
	'posts_per_page' => $atts['count'] ?? 3,
	'post_status'    => 'publish',
	'orderby'        => 'date',
	'order'          => 'DESC',
];

$button_text = $atts['button_text'];
$css_class   = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );

$query = new WP_Query( $args );

if ( $query->have_posts() ) :
	?>
	<div class="wpb_column vc_column_container vc_col-xs-12 <?php echo esc_attr( $css_class ?? null ); ?>">
		<div class="vc_column-inner">
			<div class="wpb_wrapper">
				<div class="cases-slider">
					<?php
					while ( $query->have_posts() ) :
						$query->the_post();
						$case_id = get_the_ID();
						?>
						<div class="item">
							<a class="link" href="<?php the_permalink(); ?>"></a>
							<div class="cases-head">
								<?php if ( has_post_thumbnail( $case_id ) ) : ?>
									<?php the_post_thumbnail( 'cases_tbm' ); ?>
								<?php else : ?>
									<img src="//via.placeholder.com/288x255" alt="No Image">
								<?php endif; ?>
							</div>
							<div class="cases-footer">
								<p class="category"><?php the_title(); ?></p>
								<p class="more"><?php esc_attr_e( 'More', 'ostd' ); ?></p>
							</div>
						</div>
					<?php endwhile;
					wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
	</div>
<?php
endif;
