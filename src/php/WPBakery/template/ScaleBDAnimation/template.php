<?php
/**
 * Scale BD Animation Block template.
 *
 * @package OSTD\WPBakery\component
 */

$param_text    = vc_param_group_parse_atts( $atts['param_text'] );
$css_class     = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$skb_title     = filter_var( $atts['title'], FILTER_SANITIZE_STRING ) ?? '';
$block_title   = filter_var( $atts['block_title'], FILTER_SANITIZE_STRING ) ?? '';
$skb_sub_title = filter_var( $atts['sub_title'], FILTER_SANITIZE_STRING ) ?? '';
?>
<ul class="table <?php echo esc_attr( $css_class ?? '' ); ?>">
	<li>
		<p><?php echo esc_html( $skb_title ) ?? ''; ?></p>
	</li>
	<li>
		<p><?php echo esc_html( $skb_sub_title ) ?? ''; ?></p>
		<ul class="table-sub" data-title="<?php echo esc_html( $block_title ) ?? ''; ?>">
			<li>
				<span class="loader">
					<span></span><span></span><span></span>
				</span>
				<?php if ( ! empty( $param_text ) ) : ?>
					<ul class="table-desc">
						<?php foreach ( $param_text as $item ) : ?>
							<li><?php echo wp_kses_post( $item['item'] ); ?></li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			</li>
			<li><span class="loader">
					<span></span><span></span><span></span></span>
				<?php if ( ! empty( $param_text ) ) : ?>
					<ul class="table-desc">
						<?php foreach ( $param_text as $item ) : ?>
							<li><?php echo wp_kses_post( $item['item'] ); ?></li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			</li>
			<li><span class="loader"> <span></span><span></span><span></span></span>
				<?php if ( ! empty( $param_text ) ) : ?>
					<ul class="table-desc">
						<?php foreach ( $param_text as $item ) : ?>
							<li><?php echo wp_kses_post( $item['item'] ); ?></li>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			</li>
		</ul>
	</li>
</ul>