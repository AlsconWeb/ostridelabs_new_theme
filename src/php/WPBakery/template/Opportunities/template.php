<?php
/**
 * Template Opportunities.
 *
 * @package OSTD\WPBakery\component
 */

$title         = $atts['title_job'];
$time          = $atts['time'];
$content       = $atts['description'];
$random_number = rand( 0, 1000 );
?>
<div class="opportunities-item">
	<p class="head-line"><?php echo esc_html( $title ); ?></p>
	<p class="icon-clock"><?php echo esc_html( $time ); ?></p>
	<a class="button" href="#" data-toggle="modal" data-target=".reliability-<?php echo esc_attr( $random_number ); ?>">
		<?php esc_html_e( 'Learn more', 'ostd' ); ?>
	</a>
</div>
<div class="reliability-<?php echo esc_attr( $random_number ); ?> modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<p class="title"><?php echo esc_html( $title ); ?></p>
				<p class="icon-clock"><?php echo esc_html( $time ); ?></p>
				<button class="close icon-close" type="button" data-dismiss="modal"></button>
			</div>
			<div class="modal-body">
				<?php echo wp_kses_post( $content ); ?>
			</div>
		</div>
	</div>
</div>
