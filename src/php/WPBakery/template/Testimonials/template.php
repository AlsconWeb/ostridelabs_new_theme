<?php
/**
 * Template Step by Step.
 *
 * @package OSTD\WPBakery\component
 */

$css_class          = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
$style              = ! empty( $atts['style'] ) ? (int) $atts['style'] : 1;
$testimonials_title = $atts['title'] ?? '';
$post_not_in        = $atts['post_not_in'] ?? null;

if ( 1 === $style ) {
	$args = [
		'post_type'      => 'testimonials',
		'posts_per_page' => $atts['count'] ?? 5,
		'post_status'    => 'publish',
		'post__not_in'   => explode( ',', $post_not_in ),
		'orderby'        => [
			'city_clause' => 'ostd_order',
		],
	];
} else {
	$args = [
		'post_type'      => 'testimonials',
		'posts_per_page' => 1,
		'post__in'       => [ $atts['post_id'] ],
		'post_status'    => 'publish',
	];
}
$query = new WP_Query( $args );

if ( $query->have_posts() ) :
	if ( 1 === $style ) :
		?>
		<div class="testimonials-slider <?php echo esc_attr( $css_class ?? null ); ?>">
			<?php
			while ( $query->have_posts() ) :
				$query->the_post();
				$testimonials_id = get_the_ID();
				$position        = carbon_get_post_meta( $testimonials_id, 'ostd_position' ) ?? '';
				$logo            = carbon_get_post_meta( $testimonials_id, 'ostd_logo' ) ?? '';
				?>
				<div class="testimonials-item">
					<?php if ( $logo ): ?>
						<img
								class="logo"
								src="<?php echo esc_url( wp_get_attachment_image_url( $logo ) ); ?>"
								alt="<?php echo esc_html( get_the_title( $logo ) ) ?>"/>
					<?php endif; ?>
					<div class="comment-text">
						<?php the_content(); ?>
					</div>
					<div class="user">
						<?php if ( has_post_thumbnail( $testimonials_id ) ) : ?>
							<?php the_post_thumbnail( 'thumbnail' ); ?>
						<?php else : ?>
							<img src="//via.placeholder.com/100" alt="No Image">
						<?php endif; ?>
						<div class="desc">
							<p class="author-name"><?php the_title(); ?></p>
							<p class="position"><?php echo wp_kses_post( $position ); ?></p>
						</div>
					</div>
				</div>
			<?php
			endwhile;
			wp_reset_postdata();
			?>
		</div>
	<?php else : ?>
		<?php
		while ( $query->have_posts() ) :
			$query->the_post();
			$testimonials_id = get_the_ID();
			$position        = carbon_get_post_meta( $testimonials_id, 'ostd_position' ) ?? '';
			?>
			<div class="testimonials-user <?php echo esc_attr( $css_class ?? null ); ?>">
				<h3 class="title icon-testimonials"><?php echo esc_html( $testimonials_title ); ?></h3>
				<?php the_content(); ?>
				<div class="user">
					<div class="photo">
						<?php if ( has_post_thumbnail( $testimonials_id ) ) : ?>
							<?php the_post_thumbnail( 'thumbnail' ); ?>
						<?php else : ?>
							<img src="//via.placeholder.com/100" alt="No Image">
						<?php endif; ?>
					</div>
					<div class="user-desc">
						<p class="author-name"><?php the_title(); ?></p>
						<ul class="meta-user">
							<li><?php echo wp_kses_post( $position ); ?></li>
						</ul>
					</div>
				</div>
			</div>
		<?php
		endwhile;
		wp_reset_postdata();
	endif;
	?>
<?php
endif;
