<?php
/**
 * Service List template.
 *
 * @package alexl/ostridelabs
 */

$service_items = vc_param_group_parse_atts( $atts['list_items'] );
$css_class     = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );
?>
<div class="services-items <?php echo esc_attr( $css_class ) ?? ''; ?>">
	<?php
	if ( ! empty( $service_items ) ) {
		foreach ( $service_items as $item ) {

			$image = wp_get_attachment_image_url( $item['image'], 'full' );
			$url   = vc_build_link( $item['url'] );
			?>
			<div class="item">
				<?php if ( ! empty( $image ) ) { ?>
				    <div class="img">
                        <img
                                src="<?php echo esc_url( $image ); ?>"
                                alt="<?php echo esc_attr( get_the_title( $item['image'] ) ); ?>">
                        <?php echo wp_kses_post( wpautop( $item['description'] ) ); ?>
				    </div>
				<?php } ?>
				<p class="title"><?php echo esc_html( $item['title'] ); ?></p>
				
				<a
						href="<?php echo ! empty( $url['url'] ) ? esc_url( $url['url'] ) : '#'; ?>"
						target="<?php echo ! empty( $url['url'] ) ? esc_attr( $url['target'] ) : ''; ?>"
						rel="<?php echo ! empty( $url['rel'] ) ? esc_attr( $url['rel'] ) : ''; ?>"
						class="link"></a>
			</div>
			<?php
		}
	}
	?>
</div>
