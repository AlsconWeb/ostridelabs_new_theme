<?php
/**
 * Security SVG Animation template.
 *
 * @package OSTD\WPBakery\component
 */

$css_class = vc_shortcode_custom_css_class( $atts['css'] ?? '', ' ' );

// phpcs:disable
$svg = ! empty( $atts['svg'] ) ? rawurldecode( base64_decode( wp_strip_all_tags( $atts['svg'] ) ) ) : '';
// phpcs:enable

?>

	<ul class="security">
		<?php for ( $i = 0; $i < 2; $i ++ ) : ?>
			<li class="icon-lock">
				<?php for ( $j = 0; $j < 11; $j ++ ) : ?>
					<i class="icon-star"></i>
				<?php endfor; ?>
			</li>
		<?php endfor; ?>
	</ul>
<?php
// phpcs:disable
if ( $svg ) {
	echo $svg;
}
// phpcs:enable
