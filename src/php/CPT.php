<?php
/**
 * Registration Custom Post Type.
 *
 * @package OSTD
 */

namespace OSTD;

/**
 * CPT class file.
 */
class CPT {
	/**
	 * Register custom post type.
	 *
	 * @param array $args Arguments.
	 *
	 * @return void
	 */
	public function registration_cpt( array $args ): void {
		register_post_type(
			$args['post_type_name'],
			[
				'label'         => null,
				'labels'        => [
					'name'               => $args['name'],
					'singular_name'      => $args['singular_name'],
					'add_new'            => $args['add_new'],
					'add_new_item'       => $args['add_new_item'],
					'edit_item'          => $args['edit_item'],
					'new_item'           => $args['new_item'],
					'view_item'          => $args['view_item'],
					'search_items'       => $args['search_items'],
					'not_found'          => $args['not_found'],
					'not_found_in_trash' => $args['not_found_in_trash'],
					'parent_item_colon'  => $args['parent_item_colon'],
					'menu_name'          => $args['menu_name'],
				],
				'description'   => '',
				'public'        => $args['public'],
				'menu_position' => $args['menu_name'],
				'menu_icon'     => $args['menu_icon'],
				'hierarchical'  => true,
				'supports'      => [
					'title',
					'editor',
					'excerpt',
					'revisions',
					'page-attributes',
					'thumbnail',
					'author',
				],
				'taxonomies'    => [],
				'has_archive'   => false,
				'rewrite'       => true,
				'query_var'     => true,
			]
		);


		register_taxonomy(
			'category-' . $args['post_type_name'],
			[ $args['post_type_name'] ],
			[
				'label'             => '',
				'labels'            => [
					'name'              => __( 'Category', 'ostd' ),
					'singular_name'     => __( 'Category', 'ostd' ),
					'search_items'      => __( 'Search Category', 'ostd' ),
					'all_items'         => __( 'All Categories', 'ostd' ),
					'view_item '        => __( 'View Category', 'ostd' ),
					'parent_item'       => __( 'Parent Category', 'ostd' ),
					'parent_item_colon' => __( 'Parent Category:', 'ostd' ),
					'edit_item'         => __( 'Edit Category', 'ostd' ),
					'update_item'       => __( 'Update Category', 'ostd' ),
					'add_new_item'      => __( 'Add New Category', 'ostd' ),
					'new_item_name'     => __( 'New Category Name', 'ostd' ),
					'menu_name'         => __( 'Category', 'ostd' ),
					'back_to_items'     => __( '← Back to Categories', 'ostd' ),
				],
				'description'       => '',
				'public'            => true,
				'hierarchical'      => true,
				'rewrite'           => true,
				'capabilities'      => [],
				'show_admin_column' => true,
				'show_in_rest'      => null,
				'rest_base'         => null,
			]
		);

	}
}
