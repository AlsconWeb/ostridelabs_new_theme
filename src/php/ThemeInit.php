<?php
/**
 * Themes init Ostridelabs.
 *
 * @package OSTD
 */

namespace OSTD;

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;
use OSTD\WPBakery\component\ArrowBlock;
use OSTD\WPBakery\component\Cases;
use OSTD\WPBakery\component\CloudBlock;
use OSTD\WPBakery\component\CodeBlockAnimation;
use OSTD\WPBakery\component\FAQ;
use OSTD\WPBakery\component\Glossary;
use OSTD\WPBakery\component\IconChallenges;
use OSTD\WPBakery\component\Opportunities;
use OSTD\WPBakery\component\ScaleBDAnimation;
use OSTD\WPBakery\component\SecuritySVGAnimation;
use OSTD\WPBakery\component\ServiceList;
use OSTD\WPBakery\component\ServiceTabs;
use OSTD\WPBakery\component\StepByStep;
use OSTD\WPBakery\component\SVGAnimation;
use OSTD\WPBakery\component\Testimonials;

/**
 * ThemeInit class file.
 */
class ThemeInit {

	/**
	 * ThemeInit construct.
	 */
	public function __construct() {
		$this->init_hook();
	}

	/**
	 * Init Themes hooks
	 *
	 * @return void
	 */
	public function init_hook(): void {
		add_action( 'after_setup_theme', [ $this, 'crb_load' ] );
		add_action( 'after_setup_theme', [ $this, 'theme_supports' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_theme_options_page' ] );
		add_filter( 'upload_mimes', [ $this, 'add_support_svg_uploads' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts' ] );
		add_action( 'init', [ $this, 'add_ctp' ] );
		add_action( 'vc_before_init', [ $this, 'add_wp_bakery_components' ] );
		add_action( 'show_read_minutes', [ $this, 'show_read_min' ], 10, 1 );


		add_filter( 'wp_get_attachment_image_src', [ $this, 'delete_width_height' ], 100, 4 );
		add_filter( 'nav_menu_link_attributes', [ $this, 'change_current_item' ], 10, 4 );
		add_filter( 'wpseo_canonical', [ $this, 'change_canonical_blog' ], 10, 1 );
	}

	/**
	 * Boot Carbon fields.
	 *
	 * @return void
	 */
	public function crb_load(): void {
		Carbon_Fields::boot();
	}

	/**
	 * After theme setup.
	 *
	 * @return void
	 */
	public function theme_supports(): void {
		add_theme_support(
			'custom-logo',
			[
				'height'               => 27,
				'width'                => 185,
				'flex-width'           => true,
				'flex-height'          => true,
				'header-text'          => '',
				'unlink-homepage-logo' => false,
			]
		);
		add_theme_support( 'align-wide' );

		register_nav_menus(
			[
				'header_menu'        => __( 'Header menu', 'ostd' ),
				'footer_menu'        => __( 'Footer menu', 'ostd' ),
				'footer_menu_button' => __( 'Footer menu button', 'ostd' ),
			]
		);

		add_image_size( 'cases_tbm', 478, 407, [ 'center', 'top' ] );
		add_image_size( 'blog_tbm', 370, 370, [ 'center', 'top' ] );
		add_image_size( 'blog_big', 600, 390, [ 'center', 'top' ] );
	}

	/**
	 * Add support upload svg.
	 *
	 * @param array $file_types file types.
	 *
	 * @return array
	 */
	public function add_support_svg_uploads( array $file_types ): array {

		$file_types['svg'] = 'image/svg+xml';

		return $file_types;
	}

	/**
	 * Add Theme Options page.
	 *
	 * @return void
	 */
	public function add_theme_options_page(): void {
		$basic_options_container = Container::make( 'theme_options', __( 'Theme Options', 'ostd' ) )
			->add_fields(
				[
					Field::make( 'header_scripts', 'ostd_header_script' ),
					Field::make( 'footer_scripts', 'ostd_footer_script' ),
				]
			);
		Container::make( 'theme_options', 'Header' )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'text', 'ostd_phone_number', __( 'Phone number', 'ostd' ) ),
					Field::make( 'text', 'ostd_button_text', __( 'Button text', 'ostd' ) )->set_width( 50 ),
					Field::make( 'text', 'ostd_button_link', __( 'Button link', 'ostd' ) )->set_width( 50 ),
				]
			);

		Container::make( 'theme_options', 'Footer' )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'text', 'ostd_phone_number_footer', __( 'Phone number', 'ostd' ) ),
					Field::make( 'text', 'ostd_contact_email', __( 'Contact Email', 'ostd' ) ),
					Field::make( 'textarea', 'ostd_copyright', 'Copyright' )->set_rows( 4 ),
				]
			);

		Container::make( 'theme_options', 'Social Links' )
			->set_page_parent( $basic_options_container )
			->add_fields(
				[
					Field::make( 'complex', 'ostd_socials', __( 'Social', 'ostd' ) )
						->add_fields(
							[
								Field::make( 'text', 'ostd_soc_icon_name', __( 'Icon Name', 'ostd' ) )
									->set_width( 50 ),
								Field::make( 'text', 'ostd_soc_link', __( 'Link', 'ostd' ) )->set_width( 50 ),
							]
						),
				]
			);

		Container::make( 'post_meta', __( 'Position', 'ostd' ) )
			->where( 'post_type', '=', 'testimonials' )
			->add_fields(
				[
					Field::make( 'text', 'ostd_position', __( 'Position', 'ostd' ) )->set_width( 50 ),
					Field::make( 'text', 'ostd_order', __( 'Order', 'ostd' ) )->set_attribute( 'type', 'number' )
						->set_width( 50 ),
					Field::make( 'separator', 'crb_style_options', 'Logo section' ),
					Field::make( 'image', 'ostd_logo', __( 'Logo', 'ostd' ) )
						->set_width( 50 ),
				]
			);

		Container::make( 'term_meta', __( 'Category Color', 'ostd' ) )
			->where( 'term_taxonomy', '=', 'category' )
			->add_fields(
				[
					Field::make( 'color', 'crb_color', 'Color' )
						->set_required( true ),
				]
			);

		Container::make( 'post_meta', __( 'Position', 'ostd' ) )
			->where( 'post_type', '=', 'author' )
			->add_fields(
				[
					Field::make( 'text', 'ostd_position', 'Position' )->set_autoload( true ),
				]
			);

		Container::make( 'post_meta', __( 'Author', 'ostd' ) )
			->where( 'post_type', '=', 'post' )
			->add_fields(
				[
					Field::make( 'association', 'ostd_author' )
						->set_max( 1 )
						->set_types(
							[
								[
									'type'      => 'post',
									'post_type' => 'author',
								],
							]
						),
				]
			);
	}

	/**
	 * Add Style and Scripts.
	 *
	 * @return void
	 */
	public function add_scripts(): void {
		wp_enqueue_style( 'ostd-main-style', get_stylesheet_directory_uri() . '/assets/css/main.css', [], OSTD_VERSION );
		wp_enqueue_style( 'ostd-style', get_stylesheet_directory_uri() . '/style.css', [], OSTD_VERSION );
		wp_enqueue_style( 'ostd-google-font', '//fonts.googleapis.com/css2?family=Bebas+Neue&amp;display=swap', [], OSTD_VERSION );
		wp_enqueue_style( 'ostd-html5shiv', '//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', [], '3.7.0' );
		wp_enqueue_style( 'ostd-respond', '//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', [], '1.4.2' );

		wp_style_add_data( 'ostd-html5shiv', 'conditional', 'IE 9' );
		wp_style_add_data( 'ostd-respond', 'conditional', 'IE 9' );

		wp_enqueue_script( 'ostd-waypoints', get_stylesheet_directory_uri() . '/assets/js/waypoints.js', [ 'jquery' ], OSTD_VERSION, true );
		wp_enqueue_script( 'ostd-build', get_stylesheet_directory_uri() . '/assets/js/build.js', [ 'jquery' ], OSTD_VERSION, true );
		wp_enqueue_script( 'ostd-modal', get_stylesheet_directory_uri() . '/assets/js/modal.js', [ 'jquery' ], OSTD_VERSION, true );
		wp_enqueue_script( 'ostd-main', get_stylesheet_directory_uri() . '/assets/js/main.js', [ 'jquery' ], OSTD_VERSION, true );
		wp_enqueue_script( 'ostd-clutch', '//widget.clutch.co/static/js/widget.js', [ 'jquery' ], '1.0.0', true );

		wp_localize_script(
			'ostd-main',
			'ajax',
			[
				'url' => admin_url( 'admin-ajax.php' ),
			],
		);
		wp_deregister_style( 'js_composer_front' );
	}

	/**
	 * Add WPBakery Components.
	 *
	 * @return void
	 */
	public function add_wp_bakery_components(): void {
		new StepByStep();
		new Cases();
		new Testimonials();
		new IconChallenges();
		new Opportunities();
		new SVGAnimation();
		new ArrowBlock();
		new CodeBlockAnimation();
		new SecuritySVGAnimation();
		new CloudBlock();
		new ScaleBDAnimation();
		new ServiceTabs();
		new FAQ();
		new Glossary();
		new ServiceList();
	}

	/**
	 * Register Post type.
	 *
	 * @return void
	 */
	public static function add_ctp(): void {
		$custom_post_type = new CPT();

		// Cases.
		$custom_post_type->registration_cpt(
			[
				'post_type_name'     => 'cases',
				'name'               => __( 'Cases', 'ostd' ),
				'singular_name'      => __( 'Case', 'ostd' ),
				'add_new'            => __( 'Add Case', 'ostd' ),
				'add_new_item'       => __( 'Addition Case', 'ostd' ),
				'edit_item'          => __( 'Edit Case', 'ostd' ),
				'new_item'           => __( 'New Case', 'ostd' ),
				'view_item'          => __( 'View Case', 'ostd' ),
				'search_items'       => __( 'Search Cases', 'ostd' ),
				'not_found'          => __( 'Not found Cases', 'ostd' ),
				'not_found_in_trash' => __( 'Not found in the basket', 'ostd' ),
				'parent_item_colon'  => '',
				'menu_name'          => __( 'Cases', 'ostd' ),
				'menu_position'      => 10,
				'menu_icon'          => 'dashicons-media-interactive',
				'public'             => true,
			]
		);

		// Testimonials.
		$custom_post_type->registration_cpt(
			[
				'post_type_name'     => 'testimonials',
				'name'               => __( 'Testimonials', 'ostd' ),
				'singular_name'      => __( 'Testimonial', 'ostd' ),
				'add_new'            => __( 'Add Testimonial', 'ostd' ),
				'add_new_item'       => __( 'Addition Testimonial', 'ostd' ),
				'edit_item'          => __( 'Edit Testimonial', 'ostd' ),
				'new_item'           => __( 'New Testimonial', 'ostd' ),
				'view_item'          => __( 'View Testimonial', 'ostd' ),
				'search_items'       => __( 'Search Testimonials', 'ostd' ),
				'not_found'          => __( 'Not found Testimonials', 'ostd' ),
				'not_found_in_trash' => __( 'Not found in the basket', 'ostd' ),
				'parent_item_colon'  => '',
				'menu_name'          => __( 'Testimonials', 'ostd' ),
				'menu_position'      => 15,
				'menu_icon'          => 'dashicons-format-status',
				'public'             => true,
			]
		);

		// Author.
		$custom_post_type->registration_cpt(
			[
				'post_type_name'     => 'author',
				'name'               => __( 'Author', 'ostd' ),
				'singular_name'      => __( 'Author', 'ostd' ),
				'add_new'            => __( 'Add Author', 'ostd' ),
				'add_new_item'       => __( 'Addition Author', 'ostd' ),
				'edit_item'          => __( 'Edit Author', 'ostd' ),
				'new_item'           => __( 'New Author', 'ostd' ),
				'view_item'          => __( 'View Author', 'ostd' ),
				'search_items'       => __( 'Search Author', 'ostd' ),
				'not_found'          => __( 'Not found Author', 'ostd' ),
				'not_found_in_trash' => __( 'Not found in the basket', 'ostd' ),
				'parent_item_colon'  => '',
				'menu_name'          => __( 'Author', 'ostd' ),
				'menu_position'      => 15,
				'menu_icon'          => 'dashicons-admin-users',
				'public'             => true,
			]
		);

		// Glossary.
		$custom_post_type->registration_cpt(
			[
				'post_type_name'     => 'glossary',
				'name'               => __( 'Glossary', 'ostd' ),
				'singular_name'      => __( 'Glossary', 'ostd' ),
				'add_new'            => __( 'Add Glossary', 'ostd' ),
				'add_new_item'       => __( 'Addition Glossary', 'ostd' ),
				'edit_item'          => __( 'Edit Glossary', 'ostd' ),
				'new_item'           => __( 'New Glossary', 'ostd' ),
				'view_item'          => __( 'View Glossary', 'ostd' ),
				'search_items'       => __( 'Search Glossary', 'ostd' ),
				'not_found'          => __( 'Not found Glossary', 'ostd' ),
				'not_found_in_trash' => __( 'Not found in the basket', 'ostd' ),
				'parent_item_colon'  => '',
				'menu_name'          => __( 'Glossary', 'ostd' ),
				'menu_position'      => 15,
				'menu_icon'          => 'dashicons-media-spreadsheet',
				'public'             => true,
			]
		);
	}

	/**
	 * Show Read minutes in article.
	 *
	 * @param string $content Content.
	 */
	public function show_read_min( string $content ) {
		$count_words = count( explode( ' ', $content ) );

		$minutes_read = round( $count_words / 150, 0, PHP_ROUND_HALF_UP );

		echo esc_attr( $minutes_read . __( ' min', 'ostd' ) );
	}

	/**
	 * Delete width and height.
	 *
	 * @param array  $image         Image.
	 * @param int    $attachment_id Image Id.
	 * @param string $size          Size slug.
	 * @param bool   $icon          Icon.
	 *
	 * @return array
	 */
	public function delete_width_height( $image, $attachment_id, $size, $icon ): array {
		$image[1] = '';
		$image[2] = '';

		return $image;
	}

	/**
	 * Delete url current nuv menu.
	 *
	 * If you don’t understand how the fuck I do it, then it’s all just a request of a fucking SEO.
	 *
	 * @param array  $atts  Attributes.
	 * @param object $item  Item.
	 * @param array  $args  Arguments.
	 * @param int    $depth Depth.
	 *
	 * @return mixed
	 */
	public function change_current_item( $atts, $item, $args, $depth ) {
		if ( $item->current ) {
			$url          = 'javascript:;';
			$atts['href'] = esc_url( $url );
		}

		return $atts;
	}

	/**
	 * Change canonical.
	 *
	 * If you don’t understand how the fuck I do it, then it’s all just a request of a fucking SEO.
	 *
	 * @param string $canonical Canonical.
	 *
	 * @return false|mixed|string|\WP_Error
	 */
	public function change_canonical_blog( $canonical ) {
		if ( is_paged() ) {
			return get_permalink( get_option( 'page_for_posts' ) );
		}

		return $canonical;
	}
}
