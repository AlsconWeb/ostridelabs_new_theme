<?php
/**
 * Custom Walker Nav Menu.
 *
 * @package OSTD
 */

namespace OSTD;

/**
 * Custom_Walker_Nav_Menu class file.
 */
class Custom_Walker_Nav_Menu extends \Walker_Nav_Menu {
	/**
	 * Start element.
	 *
	 * @param $output
	 * @param $item
	 * @param $depth
	 * @param $args
	 * @param $id
	 *
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = [], $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$output .= $indent . '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ItemList"';

		$classes     = empty( $item->classes ) ? [] : (array) $item->classes;
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		$output .= $class_names . '>';

		$attributes = '';
		! empty( $item->attr_title ) and $attributes .= ' title="' . esc_attr( $item->attr_title ) . '"';
		! empty( $item->target ) and $attributes .= ' target="' . esc_attr( $item->target ) . '"';
		! empty( $item->xfn ) and $attributes .= ' rel="' . esc_attr( $item->xfn ) . '"';
		! empty( $item->url ) and $attributes .= ' href="' . esc_attr( $item->url ) . '"';
		$attributes .= ' itemprop="url"';

		$item_output = $args->before;
		$item_output .= '<a ' . $attributes . '>';
		$item_output .= esc_html( $item->title ) . '</a>';
		$item_output .= '<meta itemprop="name" content="' . esc_attr( $item->title ) . '"></meta>';
		$item_output .= $args->after;

		$output .= $item_output;
	}
}