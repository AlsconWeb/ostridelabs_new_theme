<?php
/**
 * Page Template.
 *
 * @package OSTD
 */

get_header();
?>
	<div class="breadcrumbs-wrapper">
		<div class="breadcrumbs">
			<?php
			if ( function_exists( 'bcn_display' ) && ! is_front_page() ) {
				bcn_display( $return = false, $linked = true, $reverse = false, $force = false );
			}
			?>
		</div>
	</div>
	<section>
		<?php
		if ( have_posts() ) :
			while ( have_posts() ) :
				the_post();
				the_content();
			endwhile;
		endif;
		?>
	</section>
<?php
get_footer();
