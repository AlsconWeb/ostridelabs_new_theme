jQuery( document ).ready( function( $ ) {
	/**
	 * contactForm
	 */
	
	if ( $( window ).width() < 767 ) {
		var isIOS = /iPad|iPhone|iPod/.test( navigator.userAgent ) && ! window.MSStream;
		if ( isIOS ) {
			console.log( 'ios' )
			if ( $( 'section table' ).length ) {
				$( 'section table' ).mCustomScrollbar( {
					scrollButtons: true,
					axis: 'x',
					advanced: { autoExpandHorizontalScroll: true }
				} );
			}
		}
	}
} );