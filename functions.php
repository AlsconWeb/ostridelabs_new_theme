<?php
/**
 * Ostridelabs Theme.
 *
 * @package OSTD
 */

use OSTD\ThemeInit;

define( 'OSTD_VERSION', '1.5.0' );

require_once __DIR__ . '/vendor/autoload.php';

new ThemeInit();
