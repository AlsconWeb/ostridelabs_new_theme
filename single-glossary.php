<?php
/**
 * Glossary single page template.
 *
 * @package alexl/ostridelabs
 */

get_header();
?>
	<div class="breadcrumbs-wrapper">
		<div class="breadcrumbs">
			<?php
			if ( function_exists( 'bcn_display' ) && ! is_front_page() ) {
				bcn_display( $return = false, $linked = true, $reverse = false, $force = false );
			}
			?>
		</div>
	</div>
	<section>
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-flex blog-inner">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<p class="vc_custom_heading title" style="text-align: center;">
							<?php esc_html_e( 'Glossary', 'ostd' ); ?>
						</p>
					</div>
				</div>
			</div>
			<?php if ( have_posts() ) : ?>
				<?php
				while ( have_posts() ) :
					the_post();
					$post_id = get_the_ID();
					?>
					<div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-4 vc_col-md-4 vc_col-xs-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<?php if ( has_post_thumbnail( $post_id ) ) : ?>
									<?php the_post_thumbnail( 'blog_tbm', [ 'class' => 'thumbnail' ] ); ?>
								<?php else : ?>
									<img src="//via.placeholder.com/370x370" alt="No Image">
								<?php endif; ?>
								<ul class="meta">
									<li style="color:#123265">
										<p class="icon-line"><?php do_action( 'show_read_minutes', esc_html( get_the_content() ) ); ?></p>
									</li>
								</ul>
								<div class="blog">
									<p class="read-more-title"><?php esc_html_e( 'Read more', 'ostd' ); ?></p>
									<?php
									$args = [
										'post__not_in'   => [ $post_id ],
										'post_type'      => 'glossary',
										'post_status'    => 'publish',
										'posts_per_page' => 2,
									];

									$query = new WP_Query( $args );

									if ( $query->have_posts() ) :
										while ( $query->have_posts() ) :
											$query->the_post();
											$read_more_post_id = get_the_ID();
											?>
											<div class="blog-item">
												<div class="img">
													<?php if ( has_post_thumbnail( $read_more_post_id ) ) : ?>
														<?php the_post_thumbnail( 'blog_tbm' ); ?>
													<?php else : ?>
														<img src="//via.placeholder.com/253x219" alt="No Image">
													<?php endif; ?>
													<p class="glossary-title">
														<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
													</p>
												</div>
												<div class="footer-item">
													<p class="icon-line"><?php do_action( 'show_read_minutes', esc_html( get_the_content( $read_more_post_id ) ) ); ?></p>
												</div>
											</div>
										<?php endwhile; ?>
										<?php wp_reset_postdata(); ?>
									<?php else : ?>
										<p class="not-found"><?php esc_html_e( 'No other articles found in this category', 'ostd' ); ?></p>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-8 vc_col-md-8 vc_col-xs-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<h1 class="title"><?php the_title(); ?></h1>
								<p class="date"><?php esc_html_e( 'Updated ', 'ostd' ); ?><?php the_time( 'j M Y' ); ?></p>
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<?php echo do_shortcode( '[templatera id="1287"]' ); ?>
		<div id="form" class="vc_row wpb_row vc_inner vc_row-fluid gradient vc_row-o-content-middle vc_row-flex">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<?php echo do_shortcode( '[templatera id="900"]' ); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();