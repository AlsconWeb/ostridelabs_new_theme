jQuery(document).ready(function($){
    $('p').filter(function(){
        return this.innerHTML == '&nbsp;';
    }).remove();
    $('footer').waypoint(function (direction) {
        if (direction === "down") {
           $('.icon-arrow-top').addClass('hide-arrow');
        } else {
           $('.icon-arrow-top').removeClass('hide-arrow');
        }
    }, {
        offset: '100%'
    });
    var tabsClick = function(){
        $('[data-toggle="tab"]').click(function(e){
            e.preventDefault();
            var hrefEl = $(this).attr('href');
            $('.nav-tabs li').removeClass('active');
            $('.tab-pane').removeClass('active');
            $(this).parent().addClass('active');
            $(hrefEl).addClass('active');
        });
    };
    
    if($('.cases').length){ 
        $('.cases-slider').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite:false,
            responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1
              }
            }
          ]
        });
    }
    var windowHeight = $(window).height(),
        footerOffset = $('section').offset().top; 
    $(document).on("scroll",function(){
        if($(document).scrollTop() > windowHeight){
            $('.icon-arrow-top').css('opacity','1');
        }
        else{
            $('.icon-arrow-top').css('opacity','0');
        }
    });
    if($(window).width() > 768){
        tabsClick();
        if($('.tab-content.slick-initialized, .nav-tabs.slick-initialized').length){
            $('.tab-content').slick("unslick");
            $('.nav-tabs').slick("unslick");
        }
    }else{ 
        
        if($('.vc_col-lg-12 .cases-item').length){
            $('.vc_col-lg-12 .cases-item').each(function(){
                var casesPTeg = $(this).find('p:not([class])');
                $(this).find('.cases-head').append(casesPTeg)
            });
        }
        $('.tab-content').slick({
            fade: true,
            arrows:false,
            asNavFor: '.nav-tabs'
        });
        $('.nav-tabs').slick({
            asNavFor: '.tab-content',
            focusOnSelect: true
        });
    }
    if($('.business-block').length){
        $('.business-block .vc_col-sm-6').hover(function(){
            var idElBlock = $(this).attr('id')
            $(this).parents('.business-block').attr('id', 'active-'+idElBlock);
            $('.business-block .vc_col-sm-6').removeClass('active');
            $(this).addClass('active');
        })
    }
    if($('.work-svg').length){
        $('.work-svg [class*="arrow-"]').click(function(){
            var idEl = $(this).data('id')
            $(this).parent().attr('data-active', idEl);
            $('.work-block .description').removeClass('active');
            $('#'+ idEl).addClass('active');
        });
    }
    if($('.testimonials-slider').length){
        $('.testimonials-slider').slick({ 
            centerMode:true,
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            centerPadding:'0',
            focusOnSelect:true,
            prevArrow:'<i class="icon-arrow-left"></i>', 
            nextArrow:'<i class="icon-arrow-right"></i>',
            responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1, 
                    centerMode:false,
                    speed: 500,
                    fade: true,
                    cssEase: 'linear'
                }
            }]
        });
    }
    if($('.wp-pagenavi').length){
        $('.wp-pagenavi .nextpostslink').addClass('icon-arrow-right');
        $('.wp-pagenavi .previouspostslink').addClass('icon-arrow-left'); 
    }
    $('.left-colums label').each(function(){
        $(this).click(function(){
            if($(this).find("input[type='radio']").prop('checked') === true){
                $('.left-colums label').removeClass('checked');
                $(this).addClass('checked');
            }else{
                $(this).removeClass('checked');
            }
        });
        if($(this).find("input[type='radio']").prop('checked') === true){
            $(this).addClass('checked');
        }
    });
    var stepsIcon = function(){
        $('.steps .desc i').click(function(){
            if($(this).hasClass('icon-plus')){
                $(this).removeClass('icon-plus').addClass('icon-minus');
                $(this).parents('.item').find('.items').slideDown();
            }else{
                $(this).removeClass('icon-minus').addClass('icon-plus');
                $(this).parents('.item').find('.items').slideUp();
            }
        });
    }
    var steps = function(){
        if($(window).width() < 768){
           if($('.steps-items').length){
               if($('.steps-slider').length === 0){
                   $('.steps-items .item').wrapAll('<div class="steps-slider"></div>');
                   $('.steps-slider').slick({
                        infinite: false,
                        adaptiveHeight:true, 
                        nextArrow:'<button class="slick-next slick-arrow">Next</button><button class="slick-next slick-arrow">Next</button>',
                    });
               }
               
            }
        }else{
            $('.steps-slider').slick("unslick");
            if($('.steps-slider').length){
                $('.steps-items .item').unwrap();
            }
        }
       
    };
    var burger = function(){
        var heightMenu = $('header .menu').height();
        $('header .burger-menu').click(function(){
            if($(this).parents('header').hasClass('open')){
                $(this).parents('header').removeClass('open');
                $(this).parents('header').css('transform', 'translateY(0px)');
            }else{
                $(this).parents('header').addClass('open');
                $(this).parents('header').css('transform', 'translateY('+heightMenu+'px)');
            }
        }); 
    };
    burger();
    var bannerImg = function(){
        $('.banner .vc_col-lg-6 img').each(function(){
            var imgBanner = $(this);
            if($(window).width() < 768){
                if($(this).parents('.banner').find('.vc_col-lg-6:first-of-type img').length === 0){
                    $(this).parents('.banner').find('.title').before(imgBanner);
                }
            }else{
                if($(this).parents('.banner').find('.vc_col-lg-6:first-of-type img').length === 1){
                    $(this).parents('.banner').find('.vc_box_border_grey').append(imgBanner);
                }
            }
            
        });
    };
    var blogInner = function(){
        var imgThumbnail = $('.blog-inner .thumbnail'),
            metaBlog = $('.blog-inner .meta');
        if($(window).width() < 768){
            $('.blog-inner .date').after(imgThumbnail);
            $('.blog-inner h2.title').before(metaBlog);
        }else{
            $('.blog-inner .vc_col-lg-4 .wpb_wrapper').prepend(metaBlog);
            $('.blog-inner .vc_col-lg-4 .wpb_wrapper').prepend(imgThumbnail);
        }
        
    };
    var servicesTitle = function(){
        $('.services-item').each(function(){
            var titleEl = $(this).find('.title');
            if($(this).find('.wpb_raw_html .wpb_wrapper')){
                $(this).find('.wpb_raw_html .wpb_wrapper').prepend(titleEl);
            }
            if($(this).find('.wpb_text_colum .wpb_wrapper')){
                $(this).find('.vc_col-lg-8 .wpb_text_column .wpb_wrapper').prepend(titleEl);
            }
        });
    }
    var cloudBlock = function(){
        $('.cloud-block').each(function(){
            var desc = $(this).find('.vc_col-lg-4 .wpb_text_column');
            $(this).find('.vc_col-lg-8 .wpb_raw_html').after(desc);
        })
    }
    $(window).on('resize', function () {
        if($(window).width() > 768){
            
            tabsClick();
            if($('.tab-content.slick-initialized, .nav-tabs.slick-initialized').length){
                $('.nav-tabs').slick("unslick");
                $('.tab-content').slick("unslick");
            }
            if($('.vc_col-lg-12 .cases-item').length){
                $('.vc_col-lg-12 .cases-item').each(function(){
                    var casesPTeg = $(this).find('p:not([class])');
                    $(this).find('.cases-footer .title').after(casesPTeg);
                });
            }
            
        }else{
            if($('.vc_col-lg-12 .cases-item').length){
                $('.vc_col-lg-12 .cases-item').each(function(){
                    var casesPTeg = $(this).find('p:not([class])');
                    $(this).find('.cases-head').append(casesPTeg)
                });
            }
            if($('.our-services .slick-initialized').length === 0){
                $('.tab-content').slick({
                    fade: true,
                    arrows:false,
                    asNavFor: '.nav-tabs'
                });
                $('.nav-tabs').slick({
                    asNavFor: '.tab-content',
                    focusOnSelect: true
                });
            }
        }
        if($(window).width() < 768){
           
            if($('.steps').length){
                steps();
            }
            if($('.banner .vc_col-lg-6').length){
                bannerImg();
            }
            if($('.blog-inner').length){
                blogInner();
            }
        }else{
            if($('.steps').length){
                steps();
            }
            if($('.banner .vc_col-lg-6').length){
                bannerImg();
            }
            if($('.blog-inner').length){
                blogInner();
            }
        }
    });
    
    if($(window).width() < 768){
        if($('.cloud-block').length){
            cloudBlock()
        }
        if($('.services-item').length){
            servicesTitle(); 
        }
        if($('.steps').length){
             steps();
        }
        if($('.banner[style*="background-image:"]').length){
            bannerBgImg();
        }
        if($('.banner .vc_col-lg-6 img').length){
            bannerImg();
        }
        if($('.blog-inner').length){
            blogInner();
        }
    }
    $('a[href*="#"]:not([href="#"]):not([data-toggle="tab"]):not([data-toggle="modal"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });  
    if ($('.services-item').length) {
        var waypoints = $('.services-item').waypoint(function (direction) {
            if (direction === "down") {
                $(this).addClass('animation');
                if($('.st1').length){
                    setTimeout(function(){
                        $('.st1').css('fill', '#28A1E6');
                        $('.st1').css('filter', 'drop-shadow(0px 0px 4px #28A1E6)');
                        $('.dev, .ops').show(); 
                        $('.menu-svg li').css('color', '#28A1E6')
                    },1700)
                }
            } else {
                $(this).removeClass('animation');
                if($('.st1').length){
                    $('.st1').css('fill', '#0b223d');
                    $('.st1').css('filter', 'drop-shadow(0px 0px 0px #28A1E6)');
                    $('.dev, .ops').hide();
                    $('.menu-svg li').css('color', '#123265')
                }
            }
        }, {
            offset: '25%'
        });
    }
    if ($('.cloud-block').length) { 
        var waypoints = $('.cloud-block').waypoint(function (direction) {
            if (direction === "down") {
                $(this).addClass('animation');
                var countEl = $('.cloud-items').children().size();
                setInterval(function(){
                    var randomizeIt = Math.floor(Math.random()*(countEl));
                    $('.cloud-item').removeClass('active');
                    $('.cloud-item:nth-of-type('+randomizeIt+')').addClass('active');
                },1000)
            } else {
                $(this).removeClass('animation');
            }
        }, {
            offset: '25%'
        });
    }
    
});
