<?php
/**
 * 404 error template.
 *
 * @package alexl/ostridelabs
 */

get_header();
?>
	<div class="breadcrumbs-wrapper">
		<div class="breadcrumbs">
			<?php
			if ( function_exists( 'bcn_display' ) && ! is_front_page() ) {
				bcn_display( $return = false, $linked = true, $reverse = false, $force = false );
			}
			?>
		</div>
	</div>
	<section>
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex blog">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<h1 class="vc_custom_heading title" style="text-align: center;">
							<?php esc_html_e( '404', 'ostd' ); ?>
						</h1>
						<p class="vc_custom_heading title" style="text-align: center;">
							<?php esc_html_e( 'Page not found', 'ostd' ); ?>
						</p>
						
					</div>
				</div>
			</div>
			<div class="wpb_column vc_column_container vc_col-sm-8 vc_col-lg-8 vc_col-md-12 vc_col-xs-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="content-404">
							<p><?php esc_html_e( 'The page you are looking for doesn’t exist or an other error occurred. Go back, or head over to ostridelabs.com to choose a new direction', 'ostd' ); ?></p>
							<a href="<?php bloginfo( 'url' ); ?>" class="button">
								<?php esc_html_e( 'Go back ', 'ostd' ); ?>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="form" class="vc_row wpb_row vc_inner vc_row-fluid gradient vc_row-o-content-middle vc_row-flex">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<?php echo do_shortcode( '[templatera id="900"]' ); ?>
					</div>
				</div>
			</div>
		</div>
		<?php echo do_shortcode( '[templatera id="1287"]' ); ?>
	</section>
<?php
get_footer();

