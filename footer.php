<?php
/**
 * Footer Template.
 *
 * @package OSTD
 */

?>
<footer>
	<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<div class="vc_column-inner">
				<div class="wpb_wrapper">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_col-lg-6 vc_col-md-6 vc_col-sm-6 vc_col-xs-12">
							<?php
							if ( has_nav_menu( 'footer_menu' ) ) {
								wp_nav_menu(
									[
										'theme_location' => 'footer_menu',
										'container'      => '',
										'menu_class'     => 'menu dfr',
										'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									]
								);
							}
							?>
							<p class="copyright"><?php echo wp_kses_post( carbon_get_theme_option( 'ostd_copyright' ) ); ?></p>
							<?php
							if ( has_nav_menu( 'footer_menu_button' ) ) {
								wp_nav_menu(
									[
										'theme_location' => 'footer_menu_button',
										'container'      => '',
										'menu_class'     => 'menu dfr',
										'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									]
								);
							}
							?>
						</div>
						<div class="vc_col-lg-2 vc_col-md-2 vc_col-sm-2 vc_col-xs-12"> 

						</div>
						<div class="vc_col-lg-4 vc_col-md-4 vc_col-sm-4 vc_col-xs-12">
							<div class="dfc">
								<p class="head-line"><?php esc_html_e( 'Contact us', 'ostd' ); ?></p>
								<?php
								$phone_numbers = explode( ', ', carbon_get_theme_option( 'ostd_phone_number_footer' ) );
								$contact_email = carbon_get_theme_option( 'ostd_contact_email' );
								foreach ( $phone_numbers as $number ) :
									?>
									<a
											class="icon-phone"
											href="tel:<?php echo esc_attr( filter_var( $number, FILTER_SANITIZE_NUMBER_INT ) ); ?>">
										<?php echo esc_html( $number ); ?>
									</a>
								<?php endforeach; ?>
								<a
										class="icon-mail"
										href="mailto:<?php echo esc_attr( filter_var( $contact_email, FILTER_SANITIZE_EMAIL ) ); ?>">
									<?php echo esc_html( $contact_email ); ?>
								</a>
								<?php
								$socials = carbon_get_theme_option( 'ostd_socials' );
								if ( ! empty( $socials ) ) :
									?>
									<ul class="soc dfr">
										<?php foreach ( $socials as $social ) : ?>
											<li class="<?php echo esc_attr( $social['ostd_soc_icon_name'] ); ?>">
												<a
														href="<?php echo esc_url( $social['ostd_soc_link'] ); ?>"
														target="_blank" rel="noopener noreferrer nofollow "></a>
											</li>
										<?php endforeach; ?>
									</ul>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<a href="#top" class="icon-arrow-top"></a>
<?php wp_footer(); ?>
</body>
</html>
