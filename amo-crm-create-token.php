<?php
/**
 * Template Name: AmoCrm.
 * Template Post Type: page
 *
 * Create Token.
 *
 * @package OSTD
 */

get_header();
?>

<?php

get_template_part( 'src/php/myAmo/createAmoToken' );

?>

<?php
get_footer();
