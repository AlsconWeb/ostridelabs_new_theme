<?php
/**
 * Index Template.
 *
 * @package OSTD
 */

get_header();
?>
	<div class="breadcrumbs-wrapper">
		<div class="breadcrumbs">
			<?php
			if ( function_exists( 'bcn_display' ) && ! is_front_page() ) {
				bcn_display( $return = false, $linked = true, $reverse = false, $force = false );
			}
			?>
		</div>
	</div>
	<section>
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex blog">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<h1 class="vc_custom_heading title" style="text-align: center;">
							<?php esc_html_e( 'Blog', 'ostd' ); ?>
						</h1>
					</div>
				</div>
			</div>
			<?php if ( have_posts() ) : ?>
				<?php
				while ( have_posts() ) :
					the_post();
					$post_id  = get_the_ID();
					$category = get_the_category( $post_id )[0];
					$color    = carbon_get_term_meta( $category->term_id, 'crb_color' );
					?>
					<div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-4 vc_col-md-4 vc_col-xs-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="blog-item">
									<div class="img">
										<a href="<?php the_permalink(); ?>">
											<?php if ( has_post_thumbnail( $post_id ) ) : ?>
												<?php the_post_thumbnail( 'blog_tbm' ); ?>
											<?php else : ?>
												<img src="//via.placeholder.com/253x219" alt="No Image">
											<?php endif; ?>
										</a>
										<h3>
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</h3>

									</div>
									<div class="footer-item" style="color:<?php echo esc_attr( $color ); ?>">
										<h3><?php echo esc_html( $category->cat_name ) ?? ''; ?></h3>
										<p class="icon-line"><?php do_action( 'show_read_minutes', esc_html( get_the_content() ) ); ?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex blog">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<?php
						if ( function_exists( 'wp_pagenavi' ) ) {
							wp_pagenavi();
						}
						?>
					</div>
				</div>
			</div>
		</div>
		<div id="form" class="vc_row wpb_row vc_inner vc_row-fluid gradient vc_row-o-content-middle vc_row-flex">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<?php echo do_shortcode( '[templatera id="900"]' ); ?>
					</div>
				</div>
			</div>
		</div>
		<?php echo do_shortcode( '[templatera id="1287"]' ); ?>
	</section>
<?php
get_footer();
