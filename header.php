<?php
/**
 * Header Template.
 *
 * @package OSTD
 */

use OSTD\Custom_Walker_Nav_Menu;

$micro_marking = '';

if ( is_page( [ 15 ] ) ) {
	$micro_marking = 'itemscope itemtype="https://schema.org/FAQPage"';
}
?>

<!DOCTYPE html>
<html
	<?php language_attributes(); ?>
	style="<?php echo is_user_logged_in() ? "margin-top:0 !important;" : ''; ?>" <?php echo ! empty( $micro_marking ) ? $micro_marking : ''; ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?></title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<?php wp_head(); ?>
		
	<!-- Google Tag Manager -->
	<script>( function( w, d, s, l, i ) {
			w[ l ] = w[ l ] || [];
			w[ l ].push( {
				'gtm.start':
					new Date().getTime(), event: 'gtm.js'
			} );
			var f = d.getElementsByTagName( s )[ 0 ],
				j = d.createElement( s ), dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore( j, f );
		} )( window, document, 'script', 'dataLayer', 'GTM-KXF9T2Q' );</script>
	<!-- End Google Tag Manager -->
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		var fired = false;

		window.addEventListener( 'scroll', () => {
			if ( fired === false ) {
				fired = true;

				setTimeout( () => {
					( function( m, e, t, r, i, k, a ) {
						m[ i ] = m[ i ] || function() {
							( m[ i ].a = m[ i ].a || [] ).push( arguments );
						};
						m[ i ].l = 1 * new Date();
						k = e.createElement( t ), a = e.getElementsByTagName( t )[ 0 ], k.async = 1, k.src = r, a.parentNode.insertBefore( k, a );
					} )( window, document, 'script', 'https://mc.yandex.ru/metrika/tag.js', 'ym' );
					ym( 81994843, 'init', {
						clickmap: true,
						trackLinks: true,
						accurateTrackBounce: true,
						webvisor: true
					} );
				}, 1000 );
			}
		} );


	</script>
	<noscript>
		<div><img src="https://mc.yandex.ru/watch/81994843" style="position:absolute; left:-9999px;" alt=""/></div>
	</noscript> <!-- /Yandex.Metrika counter -->

	<!-- Start of Async Drift Code -->
	<script>
		'use strict';

		! function() {
			var t = window.driftt = window.drift = window.driftt || [];
			if ( ! t.init ) {
				if ( t.invoked ) return void ( window.console && console.error && console.error( 'Drift snippet included twice.' ) );
				t.invoked = ! 0, t.methods = [ 'identify', 'config', 'track', 'reset', 'debug', 'show', 'ping', 'page', 'hide', 'off', 'on' ],
					t.factory = function( e ) {
						return function() {
							var n = Array.prototype.slice.call( arguments );
							return n.unshift( e ), t.push( n ), t;
						};
					}, t.methods.forEach( function( e ) {
					t[ e ] = t.factory( e );
				} ), t.load = function( t ) {
					var e = 3e5, n = Math.ceil( new Date() / e ) * e, o = document.createElement( 'script' );
					o.type = 'text/javascript', o.async = ! 0, o.crossorigin = 'anonymous', o.src = 'https://js.driftt.com/include/' + n + '/' + t + '.js';
					var i = document.getElementsByTagName( 'script' )[ 0 ];
					i.parentNode.insertBefore( o, i );
				};
			}
		}();
		drift.SNIPPET_VERSION = '0.3.1';
		drift.load( 'nhu4n56rcrts' );
	</script>
	<!-- End of Async Drift Code -->
	<style type="text/css">
	@media only screen and (min-width: 1000px) {
	  body {
		padding-top: 134.781px;
	  }
	}
	</style>
</head>
<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) -->
<noscript>
	<iframe>height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<header id="header" class="<?php echo is_singular( 'cases' ) ? 'cases-breadcrumbs' : ''; ?>">
	<div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-content-middle vc_row-flex">
		<div class="wpb_column vc_column_container vc_col-sm-12">
			<div class="vc_column-inner">
				<div class="wpb_wrapper">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_col-lg-4 vc_col-md-4 vc_col-sm-4 vc_col-xs-4">
							<div class="burger-menu"></div>
							<?php
							if ( has_nav_menu( 'header_menu' ) ) {
								wp_nav_menu(
									[
										'theme_location' => 'header_menu',
										'container'      => '',
										'menu_class'     => 'menu dfr',
										'items_wrap'     => '<nav itemscope itemtype="http://schema.org/SiteNavigationElement"><ul itemprop="about" id="%1$s" class="%2$s" itemscope itemtype="http://schema.org/ItemList">%3$s</ul></nav>',
										'walker'         => new Custom_Walker_Nav_Menu(),
									]
								);
							}
							if ( is_singular( 'cases' ) ) {
								?>
								<div class="breadcrumbs-wrapper">
									<div class="breadcrumbs">
										<?php
										if ( function_exists( 'bcn_display' ) && ! is_front_page() ) {
											bcn_display( false, true, false, false );
										}
										?>
									</div>
								</div>
								<?php
							}
							?>
						</div>
						<div class="vc_col-lg-4 vc_col-md-4 vc_col-sm-4 vc_col-xs-4">
							<a
								class="logo"
								href="<?php echo is_front_page() ? 'javascript:;' : esc_url( get_bloginfo( 'url' ) ); ?>">
								<?php
								$logo = get_theme_mod( 'custom_logo' );
								if ( $logo ) :
									?>
									<img
										src="<?php echo esc_url( wp_get_attachment_image_url( $logo, 'full' ) ); ?>"
										alt="Logo">
								<?php endif; ?>
							</a>
						</div>
						<div class="vc_col-lg-4 vc_col-md-4 vc_col-sm-4 vc_col-xs-4">
							<div class="dfc left">
								<?php
								$button_text  = carbon_get_theme_option( 'ostd_button_text' );
								$button_link  = carbon_get_theme_option( 'ostd_button_link' );
								$phone_number = carbon_get_theme_option( 'ostd_phone_number' );
								?>
								<a
									class="button dfc"
									href="<?php echo esc_url( $button_link ); ?>">
									<?php echo esc_html( $button_text ); ?>
								</a>
								<a
									class="tel"
									href="tel:<?php echo esc_attr( filter_var( $phone_number, FILTER_SANITIZE_NUMBER_INT ) ); ?>">
									<?php echo esc_html( $phone_number ); ?>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
